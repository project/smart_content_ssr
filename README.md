# Smart Content SSR

The Smart Content SSR module adds a SSR Decision block, based on the Decision block that the Smart Content module provides. The main difference is that the SSR Decision block is rendered server-side without JavaScript, allowing it to run more efficiently and consistently.

Right now only basic conditions are handled, functionality for other conditions can be added with API hooks.

Supporting organizations: 
- [Kalamuna](https://www.drupal.org/kalamuna)
- [Pantheon](https://www.drupal.org/pantheon)
