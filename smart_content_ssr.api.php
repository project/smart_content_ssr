<?php

/**
 * @file
 * Hooks provided by the Smart Content SSR module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Evaluate a condition to be true or false.
 *
 * @param string $condition_id
 *   ID for the condition being checked.
 * @param array $attach_settings
 *   An associative array of additional options,
 *   which can include but not limited to the following elements:
 *   - 'value': Value set in segment settings, usually used for comparison
 *   - 'op': A string with the operation set in segment settings. Can include
 *       options such as 'equals', 'contains', 'starts_with', and empty'.
 *
 * @return bool
 *   TRUE if the condition is evaluated as true, or FALSE othewise.
 */
function hook_ssr_evaluate_condition($condition_id, array $attach_settings) {
  if ($condition_id == 'custom:condition') {
    switch ($attach_settings['op']) {
      // If the values equal each other.
      case 'equals':
        return $attach_settings['value'] === $attach_settings['custom']['value'];

      // If the custom value contains the value in settings.
      case 'contains':
        return strpos($attach_settings['custom']['value'], $attach_settings['value']) !== FALSE;

      // If the custom value starts with the value in settings.
      case 'starts_with':
        return strpos($attach_settings['custom']['value'], $attach_settings['value']) === 0;

      // If the custom value is empty.
      case 'empty':
        return empty($attach_settings['custom']['value']);
    }
  }

  return FALSE;
}

/**
 * Adds appropriate cache tags to ssr decision block.
 *
 * @param array $tags
 *   List of cache tags applied to the decision block.
 * @param array $conditions
 *   List of condition ids being used in the decision block.
 */
function hook_ssr_cache_tags(array &$tags, array $conditions) {
  // If condition exists, add cache tag.
  if (in_array('custom:condition', $conditions)) {
    $tags[] = 'custom.cache_tag';
  }
}

/**
 * @} End of "addtogroup hooks".
 */
